
var HelloWorldLayer = cc.Layer.extend({
    ship:null,
    bullet:null,
    alien:[],
    test:0,
    onProcess:false,
    ctor:function () {
        //////////////////////////////
        // 1. super init first
        this._super();

        /////////////////////////////
        // 2. add a menu item with "X" image, which is clicked to quit the program
        //    you may modify it.
        // ask the window size
        var size = cc.winSize;

        var GameName = new cc.LabelTTF("Alien Invasion 2.0", "Arial", 25);
        GameName.x = size.width / 6;
        GameName.y = 20;
        this.addChild(GameName, 3);
        GameName.runAction(
            cc.spawn(
                cc.tintTo(2.5,255,125,0)
            )
        );

        var square = new cc.DrawNode();
        square.drawRect(cc.p(0,0), cc.p(size.width, size.height),
           cc.color(255,255,255,255), 0.1,
           cc.color(255,255,255,255));
           this.addChild(square);

        var fireItem = new cc.MenuItemImage(
            res.FireBtn_png,
            res.FireBtnSelected_png,
            function () {
                if(this.test<=this.alien.length-1 && this.onProcess==false)
                {
                    this.onProcess = true;
                    this.bullet = new cc.Sprite(res.Bullet_png);
                    this.bullet.attr({
                    x: size.width / 2,
                    y: 50,
                    scale: 0.05
                    });
                    var position = this.getAngle(this.alien[this.test]);
                    this.ship.runAction(
                    cc.sequence(
                            cc.rotateTo(0.5, (90-position)),
                            cc.callFunc(function () {
                                this.addChild(this.bullet);
                                this.CheckCollition(this.bullet);
                            }, this, this)
                        )
                    );
				}
            }, this);
        fireItem.attr({
            x: size.width - 50,
            y: 50,
            anchorX: 0.5,
            anchorY: 0.5
        });

        var menu = new cc.Menu(fireItem);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu, 1);

        this.ship = new cc.Sprite(res.Ship_png);
        this.ship.attr({
            x: size.width / 2,
            y: 50,
            scale: 0.1
        });
        this.addChild(this.ship);
        this.CreateAlien(10);
        return true;
    },
    CreateAlien:function(index)
    {
        var size = cc.winSize;
        var localTest = [];
        for(var i=0; i<index; i++)
        {
            localTest[i] = null;
            var xPos = Math.round(Math.random()*12) + 1;
            var yPos = Math.round(Math.random()*4) + 4;
             if(i%2 == 0)
             {
                localTest[i] = new cc.Sprite(res.Alienf_png);
                localTest[i].attr({
                    x: xPos * 50,
                    y: yPos * 50,
                    scale: 0.5
                });
                localTest[i].distance = Math.round(Math.sqrt(Math.pow((localTest[i].x-this.ship.x),2)+Math.pow((localTest[i].y-this.ship.y),2)));
	         }
             else
             {
                localTest[i] = new cc.Sprite(res.Aliens_png);
                localTest[i].attr({
                    x: xPos * 50,
                    y: yPos * 50,
                    scale: 0.5
                });
                localTest[i].distance = Math.round(Math.sqrt(Math.pow((localTest[i].x-this.ship.x),2)+Math.pow((localTest[i].y-this.ship.y),2)));
	         }
	    }
        var tempStorage = null;
        for(var i=0; i<localTest.length-1; i++)
        {
            for(var j=i+1; j<localTest.length; j++)
            {
                if(localTest[i].distance > localTest[j].distance)
                {
                    tempStorage = localTest[i];
                    localTest[i] = localTest[j];
                    localTest[j] = tempStorage;
				}
			}
		}
        for(var i=0; i<localTest.length-1; i++)
        {
            if(localTest[i+1])
            {
                if((localTest[i].x == localTest[i+1].x) && (localTest[i].y == localTest[i+1].y))
                {
                    continue;
			    }
			}
            else
            {
                if((localTest[i].x == localTest[i-1].x) && (localTest[i].y == localTest[i-1].y))
                {
                    continue;
			    }
			}
            this.alien.push(localTest[i]);
		}
        for(var i=0; i<this.alien.length; i++)
        {
            this.addChild(this.alien[i]);  
		}
    },
    StartExplosion : function(position)
    {
        var explosion = new cc.Sprite(res.Explosion_png);
        explosion.attr({
        x: position.x,
        y: position.y,
        scale: 0.1
        });
        this.addChild(explosion);
        explosion.runAction(
                cc.sequence(
                    cc.scaleTo(0.5, 0.5, 0.5),
                    cc.callFunc(function () {
                    this.removeChild(this.bullet);
                    this.removeChild(explosion);
                    this.onProcess = false;
                    this.alien[this.test-1].runAction(
                    cc.sequence(
                    cc.scaleTo(0.05, 0, 0))
                    );
                    }, this, this),
                    cc.scaleTo(0.5, 0, 0)
                )
            );
        if(this.test>this.alien.length-1)
        {
            var gameOver = new cc.Sprite(res.GameOver_png);
            gameOver.attr({
            x: cc.winSize.width/2,
            y: cc.winSize.height/2,
            scale: 1
            });
            this.addChild(gameOver,1);
            gameOver.runAction(
                cc.sequence(
                    cc.scaleTo(0.1, 0.5, 0.5),
                    cc.scaleTo(2.9, 0, 0),
                    cc.callFunc(function () {
                    location.reload();
                    }, this, this)
			    )     
			);
		}
	},
    CheckCollition : function(bullet)
    {
        var size = cc.winSize;
        var position = this.getAngle(this.alien[this.test]);
        if(this.test<=this.alien.length-1)
        {
            bullet.runAction(
                cc.sequence(
                    cc.rotateTo(0, (90-position)),
                    cc.moveTo(0.7, cc.p(this.alien[this.test].x,this.alien[this.test].y)),
                    cc.callFunc(function () {
                        this.StartExplosion(this.alien[this.test-1]);
                    }, this, this)
                )
            );
            this.test = this.test+1;   
		}
	},
    getAngle : function(alien) {
          var dy = alien.y - this.ship.y;
          var dx = alien.x - this.ship.x;
          var theta = Math.atan2(dy, dx);
          theta *= 180 / Math.PI;
          return theta;
    }
});


var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});
