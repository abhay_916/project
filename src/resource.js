var res = {
    Ship_png : "res/ship.png",
    Bullet_png : "res/bullet.jpg",
    GameOver_png : "res/GameOver.png",
    Explosion_png : "res/dead.jpg",
    Alienf_png : "res/alien0.jpg",
    Aliens_png : "res/alien1.jpg",
    FireBtn_png : "res/ShootBtn.png",
    FireBtnSelected_png : "res/ShootSelected.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}